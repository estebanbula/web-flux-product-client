package com.estebanbula.webfluxclient.domain.model;

import java.util.Date;

public class Product {

    private String id;
    private String name;
    private Double price;
    private Date creationDate;
    private Category category;
    private String picture;

    public Product() {
    }

    public Product(String id, String name, Double price, Date creationDate, Category category, String picture) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.creationDate = creationDate;
        this.category = category;
        this.picture = picture;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}

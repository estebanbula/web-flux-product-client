package com.estebanbula.webfluxclient.domain.service;

import com.estebanbula.webfluxclient.domain.model.Product;
import org.springframework.http.codec.multipart.FilePart;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ProductService {

    Flux<Product> retrieveAll();
    Mono<Product> findById(String id);
    Mono<Product> saveProduct(Product product);
    Mono<Product> updateProduct(String id, Product product);
    Mono<Void> deleteProduct(String id);

    Mono<Product> upload(FilePart file, String id);
}

package com.estebanbula.webfluxclient.domain.service.impl;

import com.estebanbula.webfluxclient.domain.model.Product;
import com.estebanbula.webfluxclient.domain.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.*;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final WebClient webClient;

    @Override
    public Flux<Product> retrieveAll() {
        return webClient
                .get()
                .accept(MediaType.APPLICATION_JSON)
                .exchangeToFlux(response -> response.bodyToFlux(Product.class));
    }

    @Override
    public Mono<Product> findById(String id) {
        return webClient
                .get()
                .uri("/{productId}", Collections.singletonMap("productId", id))
                .exchangeToMono(response -> response.bodyToMono(Product.class));
    }

    @Override
    public Mono<Product> saveProduct(Product product) {
        if (Objects.isNull(product.getCreationDate())) product.setCreationDate(new Date());
        return webClient
                .post()
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(product)
                .exchangeToMono(response -> response.bodyToMono(Product.class));
    }

    @Override
    public Mono<Product> updateProduct(String id, Product product) {
        return webClient
                .put()
                .uri("/{productId}", Collections.singletonMap("productId", id))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(product)
                .exchangeToMono(response -> response.bodyToMono(Product.class));
    }

    @Override
    public Mono<Void> deleteProduct(String id) {
        return webClient
                .get()
                .uri("/{productId}", Collections.singletonMap("productId", id))
                .exchangeToMono(response -> response.bodyToMono(Void.class));
    }

    @Override
    public Mono<Product> upload(FilePart file, String id) {
        MultipartBodyBuilder part = new MultipartBodyBuilder();
        part.asyncPart("file", file.content(), DataBuffer.class)
                .headers(httpHeaders -> httpHeaders.setContentDispositionFormData("file", file.filename()));
        return webClient
                .post()
                .uri("/upload/{id}", Collections.singletonMap("id", id))
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .bodyValue(part.build())
                .exchangeToMono(response -> response.bodyToMono(Product.class));
    }
}

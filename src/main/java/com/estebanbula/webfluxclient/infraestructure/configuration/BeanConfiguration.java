package com.estebanbula.webfluxclient.infraestructure.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class BeanConfiguration {

    @Value("${config.base.product-url}")
    private String productPath;

    @Bean
    public WebClient webClient() {
        return WebClient.create(productPath);
    }
}

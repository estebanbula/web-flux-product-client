package com.estebanbula.webfluxclient.infraestructure.handler;

import com.estebanbula.webfluxclient.domain.model.Product;
import com.estebanbula.webfluxclient.domain.service.ProductService;
import com.estebanbula.webfluxclient.infraestructure.util.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class ProductHandler {

    private final ProductService productService;

    public Mono<ServerResponse> retrieveAll(ServerRequest serverRequest) {
        return ServerResponse.ok().body(productService.retrieveAll(), Product.class);
    }

    public Mono<ServerResponse> findById(ServerRequest serverRequest) {
        return productService.findById(serverRequest.pathVariable(Constants.PRODUCTS_ID_PATH))
                .flatMap(ServerResponse.status(HttpStatus.OK)::bodyValue)
                .onErrorResume(error -> {
                    WebClientResponseException errorResponse = (WebClientResponseException) error;
                    if (errorResponse.getStatusCode().equals(HttpStatus.NOT_FOUND))
                        return ServerResponse.notFound().build();
                    return Mono.error(errorResponse);
                });
    }

    public Mono<ServerResponse> saveNew(ServerRequest serverRequest) {
        return serverRequest.bodyToMono(Product.class)
                .flatMap(productService::saveProduct)
                .flatMap(ServerResponse.status(HttpStatus.CREATED)::bodyValue)
                .onErrorResume(error -> {
                    WebClientResponseException errorResponse = (WebClientResponseException) error;
                    if (errorResponse.getStatusCode().equals(HttpStatus.BAD_REQUEST))
                        return ServerResponse.badRequest()
                                .contentType(MediaType.APPLICATION_JSON)
                                .bodyValue(errorResponse.getResponseBodyAsString());
                    return Mono.error(errorResponse);
                });
    }

    public Mono<ServerResponse> updateOne(ServerRequest serverRequest) {
        return serverRequest.bodyToMono(Product.class)
                .flatMap(product ->
                        productService.updateProduct(serverRequest.pathVariable(Constants.PRODUCTS_ID_PATH), product))
                .flatMap(ServerResponse.status(HttpStatus.OK)::bodyValue)
                .onErrorResume(error -> {
                    WebClientResponseException errorResponse = (WebClientResponseException) error;
                    if (errorResponse.getStatusCode().equals(HttpStatus.NOT_FOUND))
                        return ServerResponse.notFound().build();
                    return Mono.error(errorResponse);
                });
    }

    public Mono<ServerResponse> deleteOne(ServerRequest serverRequest) {
        return productService.deleteProduct(serverRequest.pathVariable(Constants.PRODUCTS_ID_PATH))
                .flatMap(ServerResponse.status(HttpStatus.NO_CONTENT)::bodyValue)
                .onErrorResume(error -> {
                    WebClientResponseException errorResponse = (WebClientResponseException) error;
                    if (errorResponse.getStatusCode().equals(HttpStatus.NOT_FOUND))
                        return ServerResponse.notFound().build();
                    return Mono.error(errorResponse);
                });
    }

    public Mono<ServerResponse> upload(ServerRequest serverRequest) {
        return serverRequest.multipartData()
                .map(multiValueMap -> multiValueMap.toSingleValueMap().get("file"))
                .cast(FilePart.class)
                .flatMap(file -> productService.upload(file, serverRequest.pathVariable("id")))
                .flatMap(ServerResponse.status(HttpStatus.OK)::bodyValue)
                .onErrorResume(error -> {
                    WebClientResponseException errorResponse = (WebClientResponseException) error;
                    if (errorResponse.getStatusCode().equals(HttpStatus.NOT_FOUND))
                        return ServerResponse.notFound().build();
                    return Mono.error(errorResponse);
                });
    }
}

package com.estebanbula.webfluxclient.infraestructure.router;

import com.estebanbula.webfluxclient.application.ApiProperties;
import com.estebanbula.webfluxclient.infraestructure.handler.ProductHandler;
import com.estebanbula.webfluxclient.infraestructure.util.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.path;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
@RequiredArgsConstructor
public class ProductRouter {

    private final ApiProperties properties;

    @Bean
    public RouterFunction<ServerResponse> routes(ProductHandler productHandler) {
        return nest(path(properties.getPath()),
                route()
                        .GET("", productHandler::retrieveAll)
                        .GET(Constants.PRODUCTS_ID_PATH, productHandler::findById)
                        .POST("", productHandler::saveNew)
                        .PUT(Constants.PRODUCTS_ID_PATH, productHandler::updateOne)
                        .DELETE(Constants.PRODUCTS_ID_PATH, productHandler::deleteOne)
                        .POST("/upload/{id}", productHandler::upload).build());
    }
}
